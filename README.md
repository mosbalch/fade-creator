# BitMap Fade Creator

## Installation

Install the files and compile the main.cpp file


```bash
g++ -o create_fade main.cpp
```

## Usage 

Run the file and input the filename / width / height and two colors to create the fade

```bash
./create_fade
Filename:blue
Width:200
Heigth:200

Left side color RGB
Min = 0 Max = 255
Red:0
Green:0
Blue:255

Right side color RGB
Min = 0 Max = 255
Red:0
Green:255
Blue:100
```

Expected output (As seen in the exampleFade folder):

![example_fade](exampleFade/blue.bmp)
