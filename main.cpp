#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <tuple>
#include <vector>
#include <filesystem>

uint16_t _bits_per_pixel = 24;

template<typename number_type>
std::string get_number_string(number_type number){
    std::string number_string;
    char *chars = (char*)&number;
    for(int byte = 0; byte < sizeof(number); byte++){
        number_string += chars[byte];
    }
    return number_string;
}

void bitmap_header(std::ofstream &file, int pixel_array_size){
    // Size 14 Byte
    uint32_t size = pixel_array_size + 14 + 40;
    const uint32_t four_bytes_zero = 0;
    const uint32_t offset = 14 + 40;

    file << "BM";
    file << get_number_string(size);
    file << get_number_string(four_bytes_zero); // 4 bytes 0 (for the actual file size can also be zero so its zero)
    file << get_number_string(offset);

}

void dib_header(std::ofstream &file, int32_t width, int32_t height, int pixel_array_size){
    // Size 40 Bytes
    uint32_t header_size = 40;
    int32_t pixel_width = width;
    int32_t pixel_height = height;
    uint16_t color_planes = 1;
    uint16_t bits_per_pixel = _bits_per_pixel;    // common 1,4,8,16,24,32
    uint32_t compression_method = 0; // 0 -> no compression
    uint32_t image_size = pixel_array_size; // can also be zero because no compression has been chosen
    int32_t horizontal_res = 2834; //
    int32_t vertical_res = 2834; // not needed values for printing (real life printing)
    uint32_t color_number = 0;  // default
    uint32_t important_color_number = 0;    // unimportant / not used
    
    file << get_number_string(header_size);
    file << get_number_string(pixel_width);
    file << get_number_string(pixel_height);
    file << get_number_string(color_planes);
    file << get_number_string(bits_per_pixel);
    file << get_number_string(compression_method);
    file << get_number_string(image_size);
    file << get_number_string(horizontal_res);
    file << get_number_string(vertical_res);
    file << get_number_string(color_number);
    file << get_number_string(important_color_number);

}

// void hex_to_rgb(std::string hex_values, std::tuple<uint8_t, uint8_t, uint8_t> &rgb_values){
//     int hash_pos;
//     hash_pos = hex_values.find('#');
//     if(hash_pos != std::string::npos){
//         hex_values = hex_values.replace(hash_pos, 1, "");
//     }
//     int a = std::stoi(hex_values.substr(0, 2));

//     if(hex_values.length() == 6){
//         rgb_values = {std::stoi(hex_values.substr(0, 2)), std::stoi(hex_values.substr(2, 2)), std::stoi(hex_values.substr(4, 2))};
//     }
    
// } 


std::vector<std::tuple<uint8_t, uint8_t, uint8_t>> make_fade(std::ofstream &file, int32_t width, int32_t height){
    std::vector<std::tuple<uint8_t, uint8_t, uint8_t>> pixels;
    std::tuple<uint8_t, uint8_t, uint8_t> from;
    std::tuple<uint8_t, uint8_t, uint8_t> to;

    std::string red_val;
    std::string green_val;
    std::string blue_val;
    
    std::cout << "Left side color RGB\nMin = 0 Max = 255\nRed:";
    std::cin >> red_val;
    std::cout << "Green:";
    std::cin >> green_val;
    std::cout << "Blue:";
    std::cin >> blue_val;
    
    from = {std::stoi(red_val), std::stoi(green_val), std::stoi(blue_val)};

    std::cout << "\nRight side color RGB\nMin = 0 Max = 255\nRed:";
    std::cin >> red_val;
    std::cout << "Green:";
    std::cin >> green_val;
    std::cout << "Blue:";
    std::cin >> blue_val;
    
    to = {std::stoi(red_val), std::stoi(green_val), std::stoi(blue_val)};

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            pixels.push_back(std::make_tuple(std::get<2>(from) + static_cast<double>(j)/width * (std::get<2>(to) - std::get<2>(from)), 
                                             std::get<1>(from) + static_cast<double>(j)/width * (std::get<1>(to) - std::get<1>(from)), 
                                             std::get<0>(from) + static_cast<double>(j)/width * (std::get<0>(to) - std::get<0>(from))));
        }
        
    }
    return pixels;
}   

void pixel_array(std::ofstream &file, int32_t width, int32_t height, int extra_bytes, std::vector<std::tuple<uint8_t, uint8_t, uint8_t>> &pixels){
    uint8_t blue;
    uint8_t green;
    uint8_t red;

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            blue = std::get<0>(pixels[width*i + j]);
            green = std::get<1>(pixels[width*i + j]);
            red = std::get<2>(pixels[width*i + j]);
            file << get_number_string(blue);
            file << get_number_string(green);
            file << get_number_string(red);        
        }
        for (int i = 0; i < extra_bytes; i++)
        {
            uint8_t byte = 0;
            file << get_number_string(byte);
        }
    }
}

void create_picture(std::ofstream &file){
    int32_t width;
    int32_t height;

    std::cout << "Width:";
    std::cin >> width;
    
    std::cout << "Height:";
    std::cin >> height;
    std::cout << std::endl;

    int pixel_array_byte_size = std::ceil(_bits_per_pixel * width / 32.0f) * 4 * std::abs(height);

    bitmap_header(file, pixel_array_byte_size);

    dib_header(file, width, height, pixel_array_byte_size);

    // needed as every pixel line should have 4 * N bytes of data
    int extra_bytes = 4 - ((_bits_per_pixel / 8 * width) % 4);
    if (extra_bytes == 0)
    {
        extra_bytes = 0;
    }else
    {
        extra_bytes = 4 - extra_bytes;
    }
    
    std::vector<std::tuple<uint8_t, uint8_t, uint8_t>> pixels = make_fade(file, width, height);

    pixel_array(file, width, height, extra_bytes, pixels);
}

void fade(std::ofstream &file){
    create_picture(file);
}

int main(int argc, char const *argv[])
{   
 
    std::string dir = "./fades/";
    std::string name;

    if (!std::filesystem::exists(dir))
    {
        std::filesystem::create_directory(dir);
    }
    
    std::ofstream file;

    std::cout << "Filename:";
    std::cin >> name;
   
    
    //std::string temp;
    //getline(std::cin, temp);

    file.open(dir + name + ".bmp");

    if (!file.is_open())
    {
        std::cout << "Couldn't create / edit file " + dir + name + ".bmp" << std::endl;
        return -1;
    }

    fade(file);
    file.close();
    return 0;
}